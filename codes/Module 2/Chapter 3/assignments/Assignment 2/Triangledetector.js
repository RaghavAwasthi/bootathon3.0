function check() {
    var x = document.getElementById("t1");
    var y = document.getElementById("t2");
    var z = document.getElementById("t3");
    var a = +x.value;
    var b = +y.value;
    var c = +z.value;
    // When all sides of Triangle are equal
    if (a == b && b == c) {
        document.getElementById("p1").innerHTML = " This triangle is Equilateral";
    }
    // When Either sides of Triangle are equal
    else if ((a == b && a != c) || (b == c && b != a) || (a == c && c != b)) {
        document.getElementById("p1").innerHTML = "This triangle is Isoceles";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2)))
            document.getElementById("p2").innerHTML = "This triangle is also a right angled Triangle";
    }
    // When no sides of Triangle are equal
    else if (a != b && b != c && a != c) {
        document.getElementById("p1").innerHTML = "This triangle is Scalene";
        if (Math.pow(a, 2) == (Math.pow(b, 2) + Math.pow(c, 2)) || Math.pow(b, 2) == (Math.pow(a, 2) + Math.pow(c, 2)) || Math.pow(c, 2) == (Math.pow(b, 2) + Math.pow(a, 2)))
            document.getElementById("p2").innerHTML = "This triangle is also a right angle triangle";
    }
}
//# sourceMappingURL=Triangledetector.js.map