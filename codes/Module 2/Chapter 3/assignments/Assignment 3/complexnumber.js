function check() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var c = document.getElementById("t3");
    var data = a.value;
    var real;
    var imaginary;
    var i = data.indexOf("+");
    var j = data.indexOf("-");
    console.log(i);
    if ((i != -1)) {
        real = +data.substring(0, i); // Extracting real part when it is of a+ij form
        console.log(real);
        imaginary = +data.substring(i, data.length - 1);
        console.log(imaginary);
        b.value = "Real part  :" + real;
        c.value = "Imaginary part  :" + imaginary;
    }
    else if ((j != -1)) {
        real = +data.substring(0, j); // Extracting real part when it is of a-ij form
        console.log(real);
        imaginary = +data.substring(j, data.length - 1);
        console.log(imaginary);
        b.value = "Real part  :" + real;
        c.value = "Imaginary part  :" + imaginary;
    }
    else {
        var k = data.indexOf("i"); // when only Imaginary part is present
        if (k != -1) {
            imaginary = +data.substring(0, data.length - 1);
            b.value = "Real part  : 0";
            c.value = "Imaginary part  :" + imaginary;
        }
        else {
            real = +data.substring(0, data.length); // When only Real part is present
            b.value = "Real part  :" + real;
            c.value = "Imaginary part  : 0";
        }
    }
}
//# sourceMappingURL=complexnumber.js.map