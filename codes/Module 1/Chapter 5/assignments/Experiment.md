# Mining in BlockChain

## Aim
In this experiment, the user will learn about mining in blockchain i.e. how a transaction is validated and added into a blockchain. <br>
He/she will learn how the process of hashing helps in validation of a block.He/she will also get to know which miner is rewarded when a block is validated and added to the blockchain.

## Theory

### Blockchain Technology
A blockchain is basically a living list of records, called as "blocks". These blocks are connected to each other by the diverse cryptographic mechanisms. In the category of data structures, this can be related to the concept of a Linked List. In Blockchain, the initial block is known as the "Genesis Block". This naming convention is basically a major commendation to Satoshi Nakamoto. The domain of crypto-currency was pioneered by a bogus naming convention. It can be related to a random scenario of a person or a group of persons, represented by a peculiar name “Satoshi Nakamoto”. In the year 2008, for the purpose of Bitcoin this name was utilized. The technology that was used behind the Bitcoin spectrum was “Block-Chain”. Initially the structure of a block has basically 3 components namely data, hash of current block and hash of previous block.

### Mining
In terms of the block chain domain, mining is the procedure of appending transactions to an enormous distributed ledger of extant transactions. This concept is well suited for the bitcoin approach but the diverse technologies that uses the block chain approach can also perform the approach of mining as well. It allows the creation of a hash for a block of transactions that cannot be changed easily protecting the integrity approach of the block chain. The concept of mining goes really well with the other two approaches that are open ledger and distributed ledger.
![mining](https://user-images.githubusercontent.com/24982437/79059502-72bbb980-7c98-11ea-8d32-7286c567a909.jpg)

### Some Basic Algorithmic Rules Used

#### SHA-256 and ECDSA
SHA-256 or Secure Hash Algorithm-256 bit is a type of hash function which is commonly used in Blockchain. SHA-256 changes an input from the user to a string which is a mixture of numbers and letter which is created through a cryptographically secure hashing function which is almost 0% similar to the input. SHA-256 is the strongest hash function available in the current scenario and it is a successor of SHA-1.<br>
Eg:- SHA-256 hash of 'abc' will be 'ba7816bfÂ­8f01cfeaÂ­414140deÂ­5dae2223Â­b00361a3Â­96177a9cÂ­b410ff61Â­f20015ad'<br>
ECDSA stands for Elliptic Curve Digital Signature Algorithm. ECDSA consists of three parts.<br>

- Private Key
- Public Key
- Signature

<b>Private Key</b> :- It is a number in form of secret key which is known only to the person who owns it and does transactions. Private Key is a randomly generated number which is a single unsigned 256 bit integer.<br>
<b>Public Key</b> :- It is a number generated from Private Key but is not kept secret. A public Key can be determined from Private Key but Private Key cannot be determined from Public Key. A Public Key can be used to determine whether a Signature is genuine or not without requiring Private Key.<br>
<b>Signature</b> :- It is a number that confirms about a signing operation taking place. A Signature is a mathematically generated hash of the signed number and Priavte Key. A Public Key is used to determine whether the signature entered is genuine or not which provides security to the transactions.<br>
![sha-256](https://user-images.githubusercontent.com/24982437/79059503-73ece680-7c98-11ea-8b6f-560857bef7a6.png)
![ecdsa](https://user-images.githubusercontent.com/24982437/79059504-751e1380-7c98-11ea-97ef-518fd77fff0f.png)

## Procedure


### Steps of Simulator
  1.Start with the task regarding concept of mining.(If previously known, otherwise skip)<br>
        
![procedure](https://user-images.githubusercontent.com/24982437/79059565-0ab9a300-7c99-11ea-8aad-2903dca31ed2.png)<br>
        2. Match the following with the correct answer.<br>
        3. Select the first block on left side(Step number).<br>
        4. Now, select the block in right in such a way that it is the correct position on left.<br>
        5. Do the same procedure for the rest of the steps.<br>
        6. Now, after you've done matching click on "VALIDATE" button.<br>
        7. If all the answers are correct,then a popup will appear saying <span
            style="color: green;"><b>"Valid!"</b></span>.<br>
        8. If popup shows "Not Valid!" then reset the test by cicking on "RESET" button to restart the test.<br>
        9. Now click on initiate mining process to go to the next part.<br>
        10. Enter the Name and Amount (Cryptocurrency) of the sender as well as the recipient in the placeholder. <br>
        11. Click on the 'Add to block' button to complete the details of a particular user. As soon as the button is
        clicked, the details will get added to the block. <br>
        12. The illustration will take place according to the inputs given by the user.<br>
        13. Complete the same process for the next user. <br>
        14. Click on the start mining process button, to start the mining process.<br>
        15. Click on the reset button to reset all the details that were entered by the user.<br>
        16. The instruction pane will also be there to make the user understand about the basic process that is
        happening in the simulator. <br><br>





